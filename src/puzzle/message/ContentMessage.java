package puzzle.message;

import puzzle.world.Agent;

public class ContentMessage<T> extends Message {
    protected T content;

    public ContentMessage(Agent sender, MessageType type, T content) {
        super(sender, type);
        this.content = content;
    }

    public T getContent(){
        return this.content;
    }
}
