package puzzle.message;

public enum MessageType {
    INIT,
    OFFER,
    ACCEPT
}
