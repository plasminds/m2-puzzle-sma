package puzzle.message;

import puzzle.world.Agent;

public class Message {
    protected Agent sender;
    protected MessageType type;

    public Message(Agent sender, MessageType type) {
        this.sender = sender;
        this.type = type;
    }

    public Agent getSender() {
        return sender;
    }

    public MessageType getType() {
        return type;
    }

    /**
     * Sends the given message to the given receiver.
     * @param receiver the agent that can receive the message
     * @param message the message to send
     */
    public static void write(Agent receiver, Message message) {
        receiver.messages.add(message);
    }

}

