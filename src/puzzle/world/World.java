package puzzle.world;

import puzzle.utils.Conflict;
import puzzle.utils.Matrix;
import puzzle.utils.Point2D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import static java.util.function.Predicate.*;

public final class World extends Entity {

    private Matrix matrix;
    private volatile Map<Agent, Conflict> conflicts = new HashMap<>();

    public World(int dimension, int agentNumber, int interval) {
        super(interval);
        matrix = new Matrix(dimension);

        for (int i = 0; i < agentNumber; i++) {
            Point2D objective = matrix.getRandomPosition(true);
            Point2D position = matrix.getRandomPosition(false);

            Agent agent = new Agent(this, matrix.getPositionNumber(position), objective, 1000);
            matrix.set(agent, position);
            conflicts.put(agent, null);
            agent.changePath(findPath(agent));
        }
    }

    @Override
    public void run() {
        while (isRunning()) {
            try {
                printMatrix();
                if(isSat()) this.stop();
                this.worker.sleep(this.interval);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted thread");
            }
        }
    }

    @Override
    public void start() {
        super.start();
        conflicts.keySet().parallelStream().forEach(Agent::start);
    }

    @Override
    public void stop() {
        super.stop();
        conflicts.keySet().parallelStream().forEach(Agent::stop);
    }

    public boolean isSat() {
        return conflicts.keySet().stream().noneMatch(Agent::shouldMove);
    }

    public void printMatrix() {
        conflicts.forEach((agent, conflict) -> System.out.println(agent + " " + agent.getPersonality() + " " + getCurrentPosition(agent) + " " + agent.getObjective() + " " + conflict));
        matrix.print();
    }


    /*============== FUNCTIONS FOR ACTIONS ==============*/

    public synchronized boolean move(Agent agent, Point2D position) {
        if(!isFreeTile(position)) return false;
        matrix.remove(agent);
        matrix.set(agent, position);
        agent.updatePathAfterMove();
        return true;
    }

    public synchronized boolean registerConflict(Agent agent, Point2D position) {
        Agent other = matrix.get(position);
        if(other == null) return false;
        if(conflicts.get(other) != null) return false;
        Conflict conflict = new Conflict(agent, other, position);
        if(conflict.equals(agent.getLastResolvedConflict())) return false;
        conflicts.put(agent, conflict);
        conflicts.put(other, conflict);
        return true;
    }

    public void removeConflict(Agent agent) {
        agent.setLastResolvedConflict();
        conflicts.put(agent, null);
    }


    /*============== FUNCTIONS FOR PERCEPTIONS ==============*/

    public boolean isFreeTile(Point2D position) {
        return matrix.isFreePosition(position);
    }

    public Conflict getConflict(Agent agent) {
        return conflicts.get(agent);
    }

    public Queue<Point2D> findPath(Agent agent) {
        return matrix.findPath(agent);
    }

    public Queue<Point2D> findPathWithExclusion(Agent agent, Point2D... exclude) {
        return matrix.findPath(agent, new ArrayList<>(Arrays.asList(exclude)));
    }

    public List<Queue<Point2D>> findAlternativePaths(Conflict conflict, Agent client) {
        return matrix.findAlternativePaths(conflict, client);
    }

    public Point2D getCurrentPosition(Agent agent) {
        return matrix.indexOf(agent);
    }

    /*============== FUNCTIONS FOR CRITERIA (PERCEPTIONS FOR NEGOTIATION) ==============*/

    public double computeDistance(Queue<Point2D> path) {
        return path.size();
    }

    public double countFinalObjectives(Queue<Point2D> path) {
        return path.stream().filter(matrix.getGeneratedPositions()::contains).count();
    }

    public double countNearAgents(Queue<Point2D> path) {
        return path.stream()
                .map(position -> matrix.getNeighborhood(position))
                .flatMap(Collection::stream)
                .filter(not(path::contains))
                .count();
    }
}
