package puzzle.world;

import puzzle.message.ContentMessage;
import puzzle.message.Message;
import puzzle.utils.Conflict;
import puzzle.utils.Point2D;
import puzzle.world.Personalities.Personality;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import static java.util.function.Predicate.*;
import static puzzle.message.MessageType.ACCEPT;
import static puzzle.message.MessageType.INIT;
import static puzzle.message.MessageType.OFFER;
import static puzzle.world.Personalities.generateRandomType;

public class Agent extends Entity {
    public volatile Queue<Message> messages = new ConcurrentLinkedQueue<>();
    private final World world;
    private final int number;

    private final Point2D objective;
    private Queue<Point2D> bestPath;

    private Conflict currentConflict;
    private boolean isFreeTile;

    private boolean shouldInitConflict = false;
    private Conflict lastResolvedConflict = null;

    private final Personality personality = generateRandomType();
    private double acceptabilityThreshold = personality.getBaseThreshold();
    private List<Queue<Point2D>> offers = new ArrayList<>();

    public Agent(World world, int number, Point2D objective, int interval) {
        super(interval);
        this.world = world;
        this.number = number;
        this.objective = objective;
    }

    public Personality getPersonality() {
        return personality;
    }

    public Point2D getNextWantedPosition() {
        return bestPath.peek();
    }

    @Override
    public void run() {
        while (isRunning()) {
            try {
                percept();
                act();
                this.worker.sleep(this.interval);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted thread");
            }
        }
    }

    public void percept() {
        currentConflict = world.getConflict(this);
        if(shouldMove()) isFreeTile = world.isFreeTile(getNextWantedPosition());
    }

    public void act() {
        if(currentConflict != null) negotiate();
        else if(shouldMove()) advanceOrNewConflict();
    }

    public Conflict getLastResolvedConflict() {
        return lastResolvedConflict;
    }

    public void setLastResolvedConflict() {
        lastResolvedConflict = currentConflict;
    }

    public boolean shouldMove() {
        return !bestPath.isEmpty();
    }

    public Point2D getObjective() {
        return objective;
    }

    public void changePath(Queue<Point2D> newPath) {
        bestPath = newPath;
    }

    // ADVANCE
    private void advanceOrNewConflict() {
        boolean hasMoved = tryToMove();
        if(!hasMoved) registerConflict();
    }

    private boolean tryToMove() {
        if(isFreeTile) return world.move(this, getNextWantedPosition());
        return false;
    }

    public void updatePathAfterMove() {
        bestPath.remove();
    }

    private void registerConflict() {
        boolean registered = world.registerConflict(this, getNextWantedPosition());
        if(registered) shouldInitConflict = true;
    }

    // NEGOTIATE
    private void negotiate() {
        if(shouldInitConflict) initNegotiations();
        if(messages.isEmpty()) return;
        Message message = messages.remove();
        System.out.println(this + " received "+message.getType()+" from " + message.getSender());
        Agent sender = message.getSender();
        switch(message.getType()) {
            case INIT:
                Point2D senderPosition = world.getCurrentPosition(sender);
                Point2D victimNextPosition = getNextWantedPosition();

                //CASE 1: there is no conflict, the victim was going to move away
                if(bestPath.size() > 0 && !victimNextPosition.equals(senderPosition) && isFreeTile) {
                    sendAccept(sender);
                    resetConflict();
                    return;
                }

                generateOffers();
                sendOffer(sender, getRandomOffer());
                break;

            case OFFER:
                Queue<Point2D> receivedOffer = ((ContentMessage<Queue<Point2D>>)message).getContent();
                double offerUtility = computePathUtility(receivedOffer);
                double currentUtility = computePathUtility(bestPath);
                double offerValue = offerUtility + acceptabilityThreshold - currentUtility;

                Queue<Point2D> randomOffer = getRandomOffer();

                //CASE 1 & 2: offer is accepted || no more offers to propose
                if(offerValue >= 0.0 || randomOffer.isEmpty()) {
                    changePath(receivedOffer);
                    sendAccept(sender);
                    resetConflict();
                    return;
                }

                sendOffer(sender, randomOffer);
                acceptabilityThreshold += 5.0;
                break;

            case ACCEPT:
                resetConflict();
                break;
        }
    }

    private void resetConflict() {
        offers = new ArrayList<>();
        world.removeConflict(this);
        acceptabilityThreshold = personality.getBaseThreshold();
    }

    private void sendAccept(Agent agent) {
        Message.write(agent, new Message(this, ACCEPT));
    }

    private void initNegotiations() {
        Message.write(currentConflict.victim, new Message(this, INIT));
        generateOffers();
        shouldInitConflict = false;
    }

    private void generateOffers() {
        offers = world.findAlternativePaths(currentConflict, currentConflict.getOpposite(this));
    }

    private Queue<Point2D> getRandomOffer() {
        List<Queue<Point2D>> noneEmptyOffers = offers.stream()
                .filter(not(Collection::isEmpty))
                .collect(Collectors.toList());
        return (noneEmptyOffers.isEmpty())
                ? new LinkedList<>()
                : noneEmptyOffers.get(new Random().nextInt(noneEmptyOffers.size()));
    }

    private void sendOffer(Agent agent, Queue<Point2D> offer) {
        System.out.println(this + " propose offer " + offer + " to " + agent);
        Message.write(agent, new ContentMessage<>(this, OFFER, offer));
    }

    private double computePathUtility(Queue<Point2D> path) {
        return Personalities.apply(personality, world, path);
    }

    @Override
    public String toString() {
        return Integer.toString(number);
    }
}
