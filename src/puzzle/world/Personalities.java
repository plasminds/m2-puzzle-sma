package puzzle.world;

import puzzle.utils.Point2D;

import java.util.Queue;
import java.util.Random;

import static java.lang.Math.*;
import static puzzle.world.Personalities.Personality.values;

public final class Personalities {

    public enum Personality {
        EGOIST(0.0),
        BASIC(5.0),
        ALTRUISTIC(10.0);

        private final double baseThreshold;

        Personality(double baseThreshold) {
            this.baseThreshold = baseThreshold;
        }

        public double getBaseThreshold() {
            return baseThreshold;
        }
    }

    public static Personality generateRandomType() {
        return values()[new Random().nextInt(values().length)];
    }

    public static double apply(Personality type, World world, Queue<Point2D> path) {
        switch (type) {
            case EGOIST: return -sumCriteria(world, path);
            case BASIC: return -(sumCriteria(world, path) + log(sumCriteria(world, path) + 1))/2;
            case ALTRUISTIC: return -log(sumCriteria(world, path) + 1);
            default: return 0.0;
        }
    }

    private static double sumCriteria(World world, Queue<Point2D> path) {
        return world.computeDistance(path) + world.countFinalObjectives(path) + world.countNearAgents(path);
    }
}
