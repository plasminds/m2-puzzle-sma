package puzzle.world;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Entity implements Runnable {
    protected final AtomicBoolean runnable;
    protected final Thread worker;
    protected final int interval;

    public Entity(int interval) {
        this.interval = interval;
        this.runnable = new AtomicBoolean(false);
        this.worker = new Thread(this);
    }

    public void start() {
        this.runnable.set(true);
        this.worker.start();
    }

    public void stop() {
        this.runnable.set(false);
        this.worker.interrupt();
    }

    public Thread.State getWorkerState() {
        return worker.getState();
    }

    public StackTraceElement[] getWorkerStackTrace() {
        return worker.getStackTrace();
    }

    public boolean isRunning() {
        return this.runnable.get();
    }
}
