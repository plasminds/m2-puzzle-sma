package puzzle.utils;

import puzzle.world.Agent;

import java.util.Objects;

public class Conflict {
    public final Agent offender;
    public final Agent victim;
    public final Point2D in;

    public Conflict(Agent offender, Agent victim, Point2D in) {
        this.offender = offender;
        this.victim = victim;
        this.in = in;
    }

    public Agent getOpposite(Agent agent) {
        return (agent.equals(victim)) ? offender : victim;
    }

    @Override
    public String toString() {
        return "Conflict{" +
            "offender=" + offender +
            ", victim=" + victim +
            ", in=" + in +
            "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Conflict conflict = (Conflict) o;
        return offender.equals(conflict.offender) &&
            victim.equals(conflict.victim) &&
            in.equals(conflict.in);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offender, victim, in);
    }
}
