package puzzle.utils;

import java.util.Arrays;
import java.util.List;

public enum Direction {

    NORTH(0, -1),
    SOUTH(0, 1),
    EAST(1, 0),
    WEST(-1, 0);

    private final Point2D position;

    Direction(int x, int y) {
        position = new Point2D(x, y);
    }

    public Point2D getPosition() {
        return position;
    }

    public static List<Direction> getAllDirections() {
        return Arrays.asList(values());
    }

    public static Direction findDirectionByPosition(Point2D position) {
        return getAllDirections().stream()
            .filter(direction -> direction.position.equals(position))
            .findFirst()
            .get();
    }

    public static Direction findAdjacentDirection(Point2D start, Point2D end) {
        return findDirectionByPosition(end.sub(start));
    }

    public static Direction getEast(Direction direction) {
        switch(direction) {
            default:
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
        }
    }

    public static Direction getWest(Direction direction) {
        switch(direction) {
            default:
            case NORTH:
                return WEST;
            case WEST:
                return SOUTH;
            case SOUTH:
                return EAST;
            case EAST:
                return NORTH;
        }
    }

    public static Direction getSouth(Direction direction) {
        switch(direction) {
            default:
            case NORTH:
                return SOUTH;
            case SOUTH:
                return NORTH;
            case WEST:
                return EAST;
            case EAST:
                return WEST;
        }
    }
}
