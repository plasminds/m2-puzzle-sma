package puzzle.utils;

import puzzle.world.Agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.stream.Collectors;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.abs;
import static java.util.Arrays.*;
import static java.util.function.Predicate.*;
import static puzzle.utils.Direction.findAdjacentDirection;
import static puzzle.utils.Direction.getAllDirections;
import static puzzle.utils.Direction.getEast;
import static puzzle.utils.Direction.getSouth;
import static puzzle.utils.Direction.getWest;
import static puzzle.utils.Point2D.randomPosition;

public class Matrix {

    private final int dimension;
    private final Agent[][] grid;
    private final List<Point2D> generatedPositions = new ArrayList<>();

    public Matrix(int dimension) {
        this.dimension = dimension;
        this.grid = new Agent[dimension][dimension];
    }

    public List<Point2D> getGeneratedPositions() {
        return generatedPositions;
    }


    /*============== BASE FUNCTIONS ==============*/

    private Agent get(int x, int y) {
        return grid[y][x];
    }

    public Agent get(Point2D position) {
        return get(position.getX(), position.getY());
    }

    private void set(Agent agent, int x, int y) {
        grid[y][x] = agent;
    }

    public void set(Agent agent, Point2D position) {
        set(agent, position.getX(), position.getY());
    }

    public Point2D indexOf(Agent agent) {
        for (int y = 0; y < dimension; y++) {
            for (int x = 0; x < dimension; x++) {
                Agent a = get(x, y);
                if (a != null && a.equals(agent))
                    return new Point2D(x, y);
            }
        }
        return null;
    }

    public Point2D remove(Agent agent) {
        Point2D position = indexOf(agent);
        assert position != null : "Null position when removing element";

        set(null, position);
        return position;
    }

    public void print() {
        for (int y = 0; y < dimension; y++) {
            for (int x = 0; x < dimension; x++) {
                Agent agent = get(x, y);

                if (agent != null) System.out.print("[" + agent + "]");
                else System.out.print("[ ]");
            }
            System.out.println();
        }
        System.out.println();
    }


    /*============== OTHER FUNCTIONS ==============*/

    private boolean isInsidePosition(Point2D position) {
        return position.getY() > -1 &&
                position.getY() < dimension &&
                position.getX() > -1 &&
                position.getX() < dimension;
    }

    public boolean isFreePosition(Point2D position) {
        return get(position) == null;
    }

    private boolean isNotAlreadyGenerated(Point2D position) {
        return !generatedPositions.contains(position);
    }

    public Point2D getRandomPosition(boolean forObjective) {
        Point2D randomPosition;
        boolean condition;

        do {
            randomPosition = randomPosition(dimension);
            condition = forObjective
                    ? isNotAlreadyGenerated(randomPosition)
                    : isFreePosition(randomPosition);
        } while (!condition);

        if (forObjective) generatedPositions.add(randomPosition);
        return randomPosition;
    }

    public int getPositionNumber(Point2D position) {
        return 1 + dimension * position.getY() + position.getX();
    }


    /*============== AGENT-BASED FUNCTIONS ==============*/

    private Optional<Point2D> getNextPosition(Point2D position, Point2D objective, List<Point2D> excludes) {
        return getNeighborhood(position)
            .stream()
            .filter(not(excludes::contains))
            .min(Comparator.comparing(pos -> computeManhattanDistance(pos, objective)));
    }

    public List<Point2D> getNeighborhood(Point2D position) {
        return getNeighborhood(position, getAllDirections());
    }

    public List<Point2D> getNeighborhood(Point2D position, List<Direction> directions) {
        return directions
            .stream()
            .map(Direction::getPosition)
            .map(position::add)
            .filter(this::isInsidePosition)
            .collect(Collectors.toList());
    }

    private int computeManhattanDistance(Point2D start, Point2D end) {
        int distance = abs(end.getX() - start.getX()) + abs(end.getY() - start.getY());
        return (distance > 0) ? distance : MAX_VALUE;
    }

    public Queue<Point2D> findPath(Agent agent) {
        return findPath(agent, new ArrayList<>());
    }

    public Queue<Point2D> findPath(Agent agent, List<Point2D> excludes) {
        return findPath(indexOf(agent), agent.getObjective(), excludes);
    }

    public Queue<Point2D> findPath(Point2D startPos, Point2D endPos, List<Point2D> excludes) {
        Point2D iterPos = startPos;
        Queue<Point2D> queue = new LinkedList<>();

        while(computeManhattanDistance(iterPos, endPos) > 1) {
            Optional<Point2D> gnp = getNextPosition(iterPos, endPos, excludes);
            if (gnp.isPresent()) {
                iterPos = gnp.get();
                queue.add(iterPos);
                excludes.add(iterPos);
            } else {
                return new LinkedList<>();
            }
        }
        queue.add(endPos);

        return queue;
    }

    public List<Queue<Point2D>> findAlternativePaths(Conflict conflict, Agent client) {
        List<Point2D> toExclude = new ArrayList<>(asList(indexOf(conflict.offender), indexOf(conflict.victim)));

        Direction offenderDirection = findAdjacentDirection(indexOf(conflict.offender), conflict.offender.getNextWantedPosition());
        List<Direction> clientAvailableDirections = new ArrayList<>(asList(getEast(offenderDirection), getWest(offenderDirection)));
        if(client.equals(conflict.offender)) {
            clientAvailableDirections.add(getSouth(offenderDirection));
        } else {
            clientAvailableDirections.add(offenderDirection);
        }

        toExclude.removeIf(e -> e.equals(client.getObjective()));
        return getNeighborhood(indexOf(client), clientAvailableDirections)
            .stream()
            .map(s -> {
                Queue<Point2D> newPath = findPath(s, client.getObjective(), toExclude);
                if (!newPath.isEmpty()) ((LinkedList<Point2D>) newPath).addFirst(s);
                return newPath;
            })
            .collect(Collectors.toList());
    }
}
