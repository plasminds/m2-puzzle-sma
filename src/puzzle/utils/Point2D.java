package puzzle.utils;

import java.util.Random;

public class Point2D {

    private final int x;
    private final int y;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point2D add(Point2D position) {
        return new Point2D(x + position.x, y + position.y);
    }

    public Point2D sub(Point2D position) {
        return new Point2D(x - position.x, y - position.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point2D point2D = (Point2D) o;
        return x == point2D.x && y == point2D.y;
    }

    @Override
    public String toString() {
        return "(x:" + x + ", y:" + y + ")";
    }

    public static Point2D randomPosition(int dimension) {
        Random random = new Random();
        return new Point2D(random.nextInt(dimension), random.nextInt(dimension));
    }
}
