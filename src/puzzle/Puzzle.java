package puzzle;

import puzzle.world.World;

public class Puzzle {

    public static void main(String[] args) {
        new World(5, 4, 1000).start();
    }
}
